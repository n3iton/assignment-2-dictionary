%ifndef PREV_IND
%define PREV_IND 0
%endif
%macro colon 2
  %%ind: 
    dq PREV_IND
    db %1, 0
  %2:  
  %define PREV_IND %%ind  
%endmacro
