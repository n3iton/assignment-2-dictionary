global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_phrase
global parse_uint
global parse_int
global string_copy

%define STDIN 0
%define STDOUT 1
%define STDERR 2

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.count:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .count
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov edi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rsi, rsp
    mov edi, 1
    mov edx, 1
    mov eax, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov edi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rcx, rcx
    xor rax, rax
    sub rsp, 1
    mov r10, 0xA
    mov byte [rsp], al
    mov rax, rdi
.count:
    xor rdx, rdx
    div r10
    add dl, 0x30
    sub rsp, 1
    mov byte [rsp], dl
    inc rcx
    cmp rax, 0
    jne .count
.print:
    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    add rsp, rcx
    add rsp, 1
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .positive
    mov r9, rdi
    mov edi, '-'
    call print_char
    mov rdi, r9
    neg rdi
.positive:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r9, r9
    xor r8, r8
    xor rax, rax
.count:
    lea r9, [rsi + rax]
    lea r8, [rdi + rax]
    mov r8b, byte[r8]
    cmp r8b, byte[r9]
    jne .nonequal
    cmp byte[r9], 0
    je .equal
    inc rax
    jmp .count
.equal:
    mov rax, 1
    ret
.nonequal:
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov edi, 0
    mov rsi, rsp
    mov eax, 0
    mov edx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor r10, r10
.empty_space:
    push r10
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop r10
    cmp al, 0x20
    je .empty_space
    cmp al, 0x9
    je .empty_space
    cmp al, 0xA
    je .empty_space
    cmp al, 0
    je .success
.count:
    mov [rdi+r10], al
    inc r10
    cmp r10, rsi
    je .fail
    push r10
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    pop r10
    cmp al, 0
    je .success
    cmp al, 0x20
    je .success
    cmp al, 0x9
    je .success
    cmp al, 0xA
    je .success
    jmp .count
.success:
    xor rax, rax
    mov [rdi+r10], al
    mov rax, rdi
    mov rdx, r10
    ret
.fail:
    xor rax, rax
    ret

read_phrase:
        xor r10, r10
.count:
        cmp r10, rsi
        je .fail
        push r10
        push rsi
        push rdi
        call read_char
        pop rdi
        pop rsi
        pop r10
        cmp al, 0
        je .success
        cmp al, 0xA
        je .success
        mov [rdi+r10], al
        inc r10
        jmp .count
.success:
        xor rax, rax
        mov [rdi+r10], al
        mov rax, rdi
        mov rdx, r10
        ret
.fail:
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    mov r11, 10
.count:
    cmp byte [rdi], '0'
    jb .end
    cmp byte [rdi], '9'
    ja .end
    push rdx
    mul r11
    pop rdx
    add al, byte [rdi]
    sub al, '0'
    inc rdx
    inc rdi
    jmp .count
.end:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    xor r9, r9
    xor r10, r10
.sign_check:
    cmp byte [rdi], '-'
    je .signed
    cmp byte [rdi], '+'
    je .signed
    jmp .number
.signed:
    inc r10
    inc rdi
.number:
    push rdx
    call parse_uint
    pop r9
    cmp r10, 1
    jne .end
    neg rax
    inc rdx
.end:
    add rdx, r9
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.count:
    cmp rdx, 0
    jle .fail
    mov al, byte [rdi]
    mov byte [rsi], al
    cmp byte [rdi], 0
    je .fit
    dec rdx
    lea rsi, [rsi+1]
    lea rdi, [rdi+1]
    jmp .count
.fail:
    mov rax, 0
    ret
.fit:
    mov rax, r10
    ret