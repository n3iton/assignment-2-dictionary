%include "colon.inc"
section .data

colon "potato fry", first_word
db "a strip of potato typically cooked by being fried in deep fat", 0

colon "watermelon", second_word
db "large fruit with a hard green or white rind, sweet watery yellowish or red flesh", 0

colon "sugar", third_word
db "a sweet substance that is made up wholly or mostly of sucrose", 0 
